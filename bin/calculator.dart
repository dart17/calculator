
import 'dart:io';
import 'dart:math';

class Calculator {

  static int precedence(String char) {
    if(char == '+' || char == '-') {
      return 1;
    } else if (char == '*' || char == '/') {
      return 2;
    } else if (char == '%') {
      return 3;
    } else {
      return 0;
    }
  }

    static List<String> tokenize(str) {
    str = str.split('');
    int max = str.length-1;
    int i = 0;
    while (i <= max) {
      if (["*","/","%","(",")"].contains(str[i])) {
        i++;
      } else if (str[i] == " ") {
        str.removeAt(i); max--;
      } else if (["+","-"].contains(str[i])) {
        if (RegExp(r'^[A-za-z0-9)]+$').hasMatch(str[i-1])) {
          i++;
        } else {
          str[i+1] = str[i]+str[i+1]; str.removeAt(i); max--;
        }
      } else {
        if (i==0) { i++; }
        else if (RegExp(r'^[A-za-z0-9]+$').hasMatch(str[i-1])) {
          str[i] = str[i-1]+str[i]; str.removeAt(i-1); max--; i--;
        } else { i++; }
      }
    }
    return str;
  }
    static List toPostfix(List<String> infix) {
    var operators = [];
    var postfix = [];
  
    for (int i=0; i<infix.length; i++) {
      if (RegExp(r'^[A-za-z0-9]+$').hasMatch(infix[i])) {
        postfix.add(infix[i]);
      } else if (["*","/","%","+","-"].contains(infix[i])) {
        while (operators.length != 0 && operators.last == "(" &&
              precedence(postfix[i]) < precedence(operators.last)) {
                var temp = operators.last;
                operators.removeAt(operators.length-1);
                postfix.add(temp);
              }
        operators.add(infix[i]);
      } else if (infix[i] == "(") {
        operators.add(infix[i]);
      } else if (infix[i] == ")") {
        while (operators.last != "(") {
          var temp = operators.last;
          operators.removeAt(operators.length-1);
          postfix.add(temp);
        }
        operators.removeAt(operators.length-1);
      }
    }
  
    while (operators.isNotEmpty) {
      postfix.add(operators.last);
      operators.removeAt(operators.length-1);
    }
    return postfix;
  }
  
  static double evalPostfix(List postfix) {
    var values = [];
  
    for (int i=0; i<postfix.length; i++) {
      if (RegExp(r'^[A-za-z0-9]+$').hasMatch(postfix[i])) {
        values.add(double.parse(postfix[i]));
      } else {
        double right = values.last;
        values.removeAt(values.length-1);
        double left = values.last;
        values.removeAt(values.length-1);
        if (postfix[i] == "+") {
          values.add(left+right);
        } else if (postfix[i] == "-") {
          values.add(left-right);
        } else if (postfix[i] == "*") {
          values.add(left*right);
        } else if (postfix[i] == "/") {
          values.add(left/right);
        } else if (postfix[i] == "%") {
          values.add(left%right);
        }
      }
    }
    return values.first;
  }

  static double calculator(String string) {
    List<String> token = tokenize(string);
    List postfix = toPostfix(token);
    return evalPostfix(postfix);
  }
}

  void main() {
print('Input Number: ');
	String string = stdin.readLineSync()!;
  print(Calculator.calculator(string));
}


